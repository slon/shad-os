# mmalloc

В этой задаче нужно написать простую библиотеку выделения памяти. А именно вам нужно реализовать функции
```c
void *malloc(size_t size);
void free(void *ptr);
void *realloc(void *ptr, size_t size);
```

Наша библиотека будет работать на основе системных вызовов `mmap`, `munmap` и `mremap`.

Системный вызов `mmap` можно использовать не только для отображения файла в память,
но и просто для выделения памяти. Для этого нужно передать специальный флаг `MAP_ANONYMOUS`.
Также потребуется указать флаг `MAP_PRIVATE`. Доступ нужно задавать на чтение и запись,
передав `PROT_READ|PROT_WRITE` в соответствующий параметр.

Системный вызов `munmap` используется чтобы убирать замапленные ранее страницы из памяти.
Как несложно догадаться, с помощью `munmap` нужно реализовать освобождение памяти.
Существенное отличие `munmap` от `free` заключается в том, что ей нужно передать размер области.
Можно воспользоваться следующим приемом: сохранить размер выделенной памяти рядом с самой выделенной памятью.
Тогда при освобождении можно будет узнать размер.

Наконец, `realloc` нужно реализовать с помощью системного вызова `mremap`. Конечно, можно было бы аллоцировать новую память, но тогда пришлось бы копировать содержимое.
Функция `mremap` не будет копировать содержимое. Даже если адрес поменяется, ей достаточно всего лишь сделать так, чтобы новые страницы памяти отображались в старые. Разумеется, это эффективнее, чем копировать все содержимое. Чтобы `mremap` мог выделить память по другому адресу, нужно передать флаг `MREMAP_MAYMOVE`.

Системные вызовы `mmap`, `munmap` и `mremap`, а также необходимые константы, определены в файле
`syscall.h`. Ими можно пользоваться для того чтобы реализовать библиотеку выделения памяти в файле `mmalloc.c`.
