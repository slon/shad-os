#include "stream.h"

#include <stdlib.h>
#include <string.h>

void stream_initialize(struct stream *stream)
{
    stream->infile = NULL;
    stream->inbuf = NULL;
    stream->inbuflen = 0;
    stream->outfile = NULL;
    stream->outbuf = NULL;
    stream->outbuflen = 0;
    stream->outbufcap = 0;
}

void stream_free(struct stream *stream)
{
    free(stream->outbuf);
}

void stream_set_input_file(struct stream *stream, FILE* file)
{
    stream->infile = file;
}

void stream_set_input_buffer(struct stream *stream, char *buffer, size_t len)
{
    stream->infile = NULL;
    stream->inbuf = buffer;
    stream->inbuflen = len;
}

void stream_set_output_file(struct stream *stream, FILE* file)
{
    stream->outfile = file;
}

char* stream_get_buffer(struct stream *stream, size_t *len)
{
    *len = stream->outbuflen;
    return stream->outbuf;
}

void stream_write(struct stream *stream, const char *ptr, size_t len)
{
    if (stream->outfile) {
        fwrite(ptr, 1, len, stream->outfile);
    } else {
        if (stream->outbuflen + len > stream->outbufcap) {
            stream->outbufcap = (stream->outbufcap == 0) ? len * 2 : stream->outbufcap * 2;
            stream->outbuf = realloc(stream->outbuf, stream->outbufcap);
        }
        memcpy(stream->outbuf + stream->outbuflen, ptr, len);
        stream->outbuflen += len;
    }
}

size_t stream_read(struct stream *stream, char *ptr, size_t len)
{
    if (stream->infile) {
        return fread(ptr, 1, len, stream->infile);
    } else {
        size_t minlen = len > stream->inbuflen ? stream->inbuflen : len;
        memcpy(ptr, stream->inbuf, minlen);
        stream->inbuf += minlen;
        stream->inbuflen -= minlen;
        return minlen;
    }
}

char* read_file(FILE *file, size_t *len)
{
    char *buffer = malloc(256);
    *len = 0;
    size_t cap = sizeof(buffer);
    size_t nread;
    while ((nread = fread(buffer + *len, 1, cap - *len, file)) > 0) {
        *len += nread;
        if (*len == cap) {
            cap *= 2;
            buffer = realloc(buffer, cap);
        }
    }
    return buffer;
}
