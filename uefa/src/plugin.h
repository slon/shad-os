#pragma once

#include "stream.h"

#include <stdio.h>

struct plugin {
    // `plugin_load` should setup these pointers:
    void (*encode)(struct stream*);
    int (*decode)(struct stream*);
    // Add more fields here:
    // ...
};

// Load plugin into memory from a given file.
// On success, return 0. On failure, return nonzero.
int plugin_load(struct plugin *plugin, FILE *file);

// Free all resources associated with plugin.
void plugin_free(struct plugin *plugin);
