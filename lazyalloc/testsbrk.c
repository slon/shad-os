#include "types.h"
#include "user.h"

#define GB (1<<30)
#define MB (1<<20)

void assert(int b)
{
  if (!b) {
    printf(0, "FAILED\n");
    exit();
  }
}

void test_w(char *p, int size)
{
  int *pi = (int*) p;
  int i;
  for (i = 0; i < size / sizeof(int); i++)
    pi[i] = i;
}

void test_r(char *p, int size)
{
  int *pi = (int*) p;
  int i;
  for (i = 0; i < size / sizeof(int); i++)
    assert(pi[i] == i);
}

void test_rw(char *p, int size)
{
  test_w(p, size);
  test_r(p, size);
}

void test_large()
{
  assert(sbrk(0x7fffffff) == (char*) -1);
}

void test_simple()
{
  char *heap = sbrk(0);
  sbrk(1000);
  test_rw(heap, 1000);
}

void test_far()
{
  char *heap = sbrk(0);
  sbrk(GB);
  test_rw(heap + GB - 1000, 1000);
  sbrk(-GB);
  assert(heap == sbrk(0));
}

void test_toofar()
{
  int p = fork();
  assert(p >= 0);
  if (!p) {
    char *heap = sbrk(0);
    sbrk(10*MB);
    test_rw(heap + 10 * MB, 4);
    printf(0, "FAILED\n");
  }
  while(wait() != p);
}

void test_multifar()
{
  char *heap = sbrk(0);
  int i;
  for (int i = 0; i < 3; i++) {
    int p = fork();
    assert(p >= 0);
    if (!p) {
        sbrk(GB);
        test_rw(heap + GB - 1000, 1000);
        sleep(2);
        exit();
    }
  }

  i = 0;
  do {
    if (wait() > 0) i++;
  } while (i < 3);

  assert(heap == sbrk(0));
}

void test_fork()
{
  char *heap = sbrk(0);
  int p;
  sbrk(GB);
  test_rw(heap + GB - 1000, 1000);
  p = fork();
  assert(p >= 0);
  test_r(heap + GB - 1000, 1000);
  if (!p) {
    exit();
  } else {
    while (wait() != p);
  }
  sbrk(-GB);
  assert(heap == sbrk(0));
}

void test_oom()
{
  int p = fork();
  assert(p >= 0);
  if (!p) {
    char *heap = sbrk(0);
    sbrk(GB);
    test_rw(heap, GB);
    printf(0, "FAILED\n");
  }
  while(wait() != p);
}

void test_dealloc()
{
  char *heap = sbrk(0);
  int i;
  for (i = 0; i < 5; i ++) {
    int j;
    sbrk(512*MB);
    for (j = 0; j < 512; j += 4) {
        test_rw(heap + j*MB, MB);
    }
    sbrk(-512*MB);
    assert(heap == sbrk(0));
  }
}

int main(int argc, char *argv[])
{
    test_large();
    test_simple();
    test_far();
    test_toofar();
    test_multifar();
    test_fork();
    test_oom();
    test_dealloc();
    printf(0, "PASSED\n");
    exit();
}
